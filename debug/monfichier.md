```dot
digraph G {
    rankdir=LR
    TerreDotTest1 [peripheries=2]
    Mars
    TerreDotTest1 -> Mars
}
```

???+ note "Open Block with toto1"

    ```dot
    digraph G {
        rankdir=LR
        TerreDotTest2 [peripheries=2]
        Mars
        TerreDotTest2 -> Mars
    }
    ```

???- note "Collapsed Block with toto2" 

    ```dot
    digraph G {
        rankdir=LR
        TerreDotTest3 [peripheries=2]
        Mars
        TerreDotTest3 -> Mars
    }
    ```

```dot
digraph G {
    rankdir=LR
    TerreDotTest4 [peripheries=2]
    Mars
    TerreDotTest4 -> Mars
}
```

TRANSITION

```graphviz dot un.svg
digraph G {
    rankdir=LR
    TerreGraphTest1 [peripheries=2]
    Mars
    TerreGraphTest1 -> Mars
}
```

???+ note "Open Block with toto"

    ```graphviz dot deux.svg
    digraph G {
        rankdir=LR
        TerreGraphTest2 [peripheries=2]
        Mars
        TerreGraphTest2 -> Mars
    }
    ```


???- note "Open Block with toto"

    ```graphviz dot trois.svg
    digraph G {
        rankdir=LR
        TerreGraphTest3 [peripheries=2]
        Mars
        TerreGraphTest3 -> Mars
    }
    ```

```graphviz dot quatre.svg
digraph G {
    rankdir=LR
    TerreGraphTest4 [peripheries=2]
    Mars
    TerreGraphTest4 -> Mars
}
```
